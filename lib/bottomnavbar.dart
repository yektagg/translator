import 'package:flutter/material.dart';
import 'package:translator/appcolors.dart';
import 'package:translator/statecontainer.dart';

class AppBottomNavBar extends StatefulWidget {
  _AppBottomNavBarState createState() => _AppBottomNavBarState();
}

class _AppBottomNavBarState extends State<AppBottomNavBar> {
  int selectedIndex = 0;
  List<NavigationItem> items = [
    NavigationItem(
      Icon(Icons.list),
      Text(
        "Doing",
        style: TextStyle(color: AppColors.purple, fontWeight: FontWeight.w600),
      ),
    ),
    NavigationItem(
      Icon(Icons.done_all),
      Text(
        "Done",
        style: TextStyle(color: AppColors.purple, fontWeight: FontWeight.w600),
      ),
    ),
    NavigationItem(
      Icon(Icons.settings),
      Text(
        "Settings",
        style: TextStyle(color: AppColors.purple, fontWeight: FontWeight.w600),
      ),
    ),
  ];

  Widget buildItem(NavigationItem item, bool isSelected) {
    selectedIndex = StateContainer.of(context).pageIndex;
    return AnimatedContainer(
      duration: Duration(milliseconds: 300),
      curve: Curves.ease,
      padding:
          EdgeInsets.symmetric(horizontal: isSelected ? 20 : 10, vertical: 10),
      decoration: BoxDecoration(
        color:
            isSelected ? AppColors.purple.withOpacity(0.15) : Colors.transparent,
        borderRadius: BorderRadius.circular(100),
      ),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          IconTheme(
            data: IconThemeData(
              color: isSelected ? AppColors.purple : AppColors.grey,
              size: 24,
            ),
            child: item.icon,
          ),
          isSelected
              ? Container(
                  margin: EdgeInsets.only(left: 5),
                  child: item.title,
                )
              : SizedBox(),
        ],
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    selectedIndex = StateContainer.of(context).pageIndex;
    return Container(
      padding: EdgeInsets.only(bottom: MediaQuery.of(context).padding.bottom),
      height: 60 + MediaQuery.of(context).padding.bottom,
      width: MediaQuery.of(context).size.width,
      decoration: BoxDecoration(
        color: Colors.white,
        boxShadow: [
          BoxShadow(
            color: AppColors.purple.withOpacity(0.2),
            offset: Offset(0, -5),
            blurRadius: 15,
          ),
        ],
      ),
      child: Container(
        padding: EdgeInsets.symmetric(horizontal: 30),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: items.map((item) {
            var itemIndex = items.indexOf(item);
            return ClipRRect(
              borderRadius: BorderRadius.circular(100),
              child: Material(
                color: Colors.transparent,
                child: GestureDetector(
                  onTap: () {
                    setState(() {
                      selectedIndex = itemIndex;
                      StateContainer.of(context).pageIndex = selectedIndex;
                      StateContainer.of(context).updatePage(selectedIndex);
                    });
                  },
                  child: Container(
                    padding: EdgeInsets.symmetric(horizontal: MediaQuery.of(context).size.width*0.04, vertical: 6),
                    child: buildItem(item, selectedIndex == itemIndex),
                  ),
                ),
              ),
            );
          }).toList(),
        ),
      ),
    );
  }
}

class NavigationItem {
  final Icon icon;
  final Text title;
  NavigationItem(this.icon, this.title);
}
