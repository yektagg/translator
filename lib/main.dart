import 'package:flutter/material.dart';
import 'package:translator/appcolors.dart';
import 'dart:async';
import 'dart:convert';
import 'package:http/http.dart' as http;
import 'package:translator/bottomnavbar.dart';
import 'package:translator/statecontainer.dart';

void main() => runApp(StateContainer(child: MyApp(),));

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Translator',
      theme: ThemeData(),
      home: MyHomePage(),
      debugShowCheckedModeBanner: false,
    );
  }
}

class TranslationItem {
  String key;
  String description;

  TranslationItem(this.key, this.description);
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key}) : super(key: key);
  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  int currentPage = 0;
  // Form Key
  List nodeList = [];
  List keyList = [];
  int totalTranslationItemCount;
  int totalTranslatedItemCount = 0;
  double translationRatio = 0.0;
  int translationPercentage = 0;

  // Map of translated data/keys
  Map<String, String> _translationMap = Map();

  Future<void> _prefillWithLocale(String locale) async {
    _translationMap.forEach((key, v) {
      setState(() {
        _translationMap[key] = "";
      });
    });
    http.Response data = await http.get(
        "https://raw.githubusercontent.com/BananoCoin/kalium_wallet_flutter/master/lib/l10n/intl_$locale.arb");
    Map<String, dynamic> arbJson = json.decode(data.body);
    arbJson.forEach((key, v) {
      if (!key.startsWith("@")) {
        setState(() {
          _translationMap[key] = v;
        });
      }
    });
  }

  // Get the data to be translated
  Future<List<TranslationItem>> _getTranslationItems() async {
    http.Response data = await http.get(
        "https://raw.githubusercontent.com/BananoCoin/kalium_wallet_flutter/master/lib/l10n/intl_messages.arb");
    Map<String, dynamic> arbJson = json.decode(data.body);
    List<TranslationItem> translationItems = [];
    arbJson.forEach((key, value) {
      if (value is String && !key.startsWith("@")) {
        translationItems.add(TranslationItem(key, value));
        _translationMap.putIfAbsent(key, () => "");
      }
    });
    totalTranslationItemCount = translationItems.length;
    translationRatio = totalTranslatedItemCount / totalTranslationItemCount;
    translationPercentage = (translationRatio * 100).toInt();

    // TODO Simulate prefilling data with a locale - this would prolly be done by a user-action
    _prefillWithLocale("tr");

    return translationItems;
  }

  @override
  Widget build(BuildContext context) {
    currentPage = StateContainer.of(context).pageIndex;
    // Home page
    return Scaffold(
      backgroundColor: Colors.white,
      bottomNavigationBar: AppBottomNavBar(),
      body: GestureDetector(
        onTap: () {
          FocusScope.of(context).requestFocus(FocusNode());
        },
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          children: <Widget>[
            Container(
              padding: EdgeInsets.only(
                top: MediaQuery.of(context).padding.top,
              ),
              decoration: BoxDecoration(
                color: Colors.white,
                boxShadow: [
                  BoxShadow(
                    color: AppColors.purple.withOpacity(0.2),
                    offset: Offset(0, 5),
                    blurRadius: 15,
                  )
                ],
              ),
              child: Container(
                margin:
                    EdgeInsets.only(left: 40, right: 40, bottom: 40, top: 20),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Row(
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: <Widget>[
                        Text(
                          "KALIUM ",
                          style: TextStyle(
                            fontSize: 24,
                            fontWeight: FontWeight.w900,
                            color: AppColors.blueishDarkGrey,
                          ),
                        ),
                        Text(
                          "(Turkish)",
                          style: TextStyle(
                            fontSize: 24,
                            fontWeight: FontWeight.w500,
                            color: AppColors.purple,
                          ),
                        ),
                      ],
                    ),
                    Container(
                      margin: EdgeInsets.only(top: 10),
                      child: Stack(
                        children: <Widget>[
                          Container(
                            width: MediaQuery.of(context).size.width - 80,
                            height: 30,
                            decoration: BoxDecoration(
                              color: AppColors.grey,
                              borderRadius: BorderRadius.circular(4),
                            ),
                          ),
                          Container(
                            width: (MediaQuery.of(context).size.width - 80) *
                                translationRatio,
                            height: 30,
                            alignment: Alignment(0, 0),
                            decoration: BoxDecoration(
                              color: AppColors.purple,
                              borderRadius: BorderRadius.circular(4),
                            ),
                            child: Text(
                              "$translationPercentage%",
                              style: TextStyle(
                                  fontSize: 12,
                                  fontWeight: FontWeight.w500,
                                  color: translationPercentage < 10
                                      ? Colors.transparent
                                      : Colors.white),
                            ),
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
              ),
            ),
            Expanded(
              child: PageView(
                onPageChanged: (int index){
                  setState(() {
                    StateContainer.of(context).pageIndex = index;
                  });
                },
                controller: StateContainer.of(context).pageController,
                children: <Widget>[
                  FutureBuilder(
                    future: _getTranslationItems(),
                    builder: (BuildContext context, AsyncSnapshot snapshot) {
                      if (snapshot.data == null) {
                        return Container(
                          child: Center(
                            child: Text("Loading"),
                          ),
                        );
                      } else {
                        return ListView.builder(
                          itemCount: snapshot.data.length,
                          itemBuilder: (BuildContext context, int index) {
                            return _buildListItem(snapshot.data[index], _translationMap[snapshot.data[index].key], index);
                          },
                        );
                      }
                    },
                  ),
                  Container(
                    child: Center(child: Icon(Icons.done_all, size:40, color:AppColors.purple)),
                  ),
                  Container(
                    child: Center(child: Icon(Icons.settings, size:40, color:AppColors.purple)),
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }

  Widget _buildListItem(TranslationItem item, String prefill, int index) {
    GlobalKey key = GlobalKey();
    FocusNode node = FocusNode();

    nodeList.add(node);
    keyList.add(key);
 
    return Container(
      padding: EdgeInsets.symmetric(vertical: 30),
      margin: EdgeInsets.only(left: 40, right: 40),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Container(
            margin: EdgeInsets.only(left: 7, right: 7),
            child: Text(
              item.description,
              textAlign: TextAlign.left,
              style: TextStyle(
                fontSize: 20,
                fontWeight: FontWeight.w800,
                color: AppColors.blueishDarkGrey,
              ),
            ),
          ),
          Container(
            margin: EdgeInsets.only(top: 15),
            decoration: BoxDecoration(
              color: AppColors.grey.withOpacity(0.1),
              border: Border.all(color: AppColors.grey),
              borderRadius: BorderRadius.circular(4),
            ),
            child: Column(
              children: <Widget>[
                TextFormField(
                    initialValue: prefill,
                    key: keyList[index],
                    focusNode: nodeList[index],
                    maxLines: null,
                    keyboardType: TextInputType.text,
                    textInputAction: TextInputAction.next,
                    textCapitalization: TextCapitalization.sentences,
                    decoration: InputDecoration(
                      hintText: "Enter your translation here",
                      contentPadding:
                          EdgeInsets.symmetric(horizontal: 10, vertical: 15),
                      border: InputBorder.none,
                      hintStyle: TextStyle(
                        fontSize: 16.0,
                        fontWeight: FontWeight.w100,
                        color: AppColors.grey,
                      ),
                    ),
                    textAlign: TextAlign.left,
                    style: TextStyle(
                      fontWeight: FontWeight.w500,
                      fontSize: 16.0,
                      color: AppColors.blueishDarkGrey,
                    ),
                    onEditingComplete: () {
                      FocusScope.of(context).requestFocus(nodeList[index + 1]);
                      Scrollable.ensureVisible(keyList[index].currentContext,
                          curve: Curves.ease);
                      setState(() {
                        totalTranslatedItemCount++;
                        translationRatio = totalTranslatedItemCount /
                            totalTranslationItemCount;
                        translationPercentage =
                            (translationRatio * 100).toInt();
                      });
                    }),
              ],
            ),
          ),
        ],
      ),
    );
  }
}